﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe.Data
{
    public class GameDbContextFactory : IDesignTimeDbContextFactory<GameDbContext>
    {
        public GameDbContext CreateDbContext(string[] args)
        {
            var databaseConnectionString = "Data Source=GERRY-WIN81;Initial Catalog=TicTacToe;User Id=sa;Password=Sqlmetodo2012;MultipleActiveResultSets=True";

            var optionsBuilder = new DbContextOptionsBuilder<GameDbContext>();
            optionsBuilder.UseSqlServer(databaseConnectionString);
            return new GameDbContext(optionsBuilder.Options);
        }
    }
}
